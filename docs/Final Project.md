# FINAL PROJECT

Vous pouvez suivre l'ensemble du travail accompli pour le projet final sur cette page. Alors dans un premier temps je me suis questionner sur comment amorcer la réalisation de mon objet. Je me suis donc penché sur l'historique lié à mon objet de référence et ensuite définir une marche à suivre afin de cadrer mon action. Bonne lecture!

## Introduction - La Panton Chair

Lors de notre première journée pour le cours d' _Architecture & Design_, nous avons été amené à visiter le musée **ADAM museum Art & Design**. Nous devions choisir un objet de l'exposition qui nous inspirait. Mon choix s'est porté sur la **PANTON CHAIR**, véritable classique de design. Pensé comme un seul objet et non comme plusieurs éléments imbriqué, cette réalisation m'a particulièrement touché. Cette fusion de la chaise où les éléments d'assise, le dossier et les piètements ne forment plus qu'un seul objet marque son carractère révolutionaire pour l'époque. 

![](images/PANTON_CHAIR.jpg)

Etant petit, nous passions une partie de nos vacances au sein la maison familiale dans laquelle se trouvaient précisément ces chaises comme en atteste la photo suivante. C'est donc au delà de l'aspect purement technique et esthétique que mon attrait pour cet objet s'est révélé. Me rappelant des souvenirs d'enfance, il me semblait naturel de me tourner vers ce projet.

Par ailleurs, l'objet fait aussi référence au monde de l'enfance de part son carractère plastique et aux couleurs vives employées. Le parrallèle se fait naturellement avec toboggans et autres strucures que nous pouvions retrouver dans les parcs. C'est donc pourquoi cette chaise à connu un succès aussi retentissant. Objet de design et utile, cette chaise s'adresse à tout type d'âge et tout type d'utilisation. Son carractère polyvalent et sa simplicité lui permet de s'insérer dans tout type d'environnement allant même jusqu'à être utilisé au sein d'une église.

Liste de mots clefs liés à la Panton Chair pouvant nous aider dans le projet : Plastique - Courbes - Porte-à-faux - Mouvement - Couleurs vives - Fusion - Elégance - Souplesse - Moule - Resistance - ...

## PANTON Verner

![](images/Verne_Panton.jpg)

Verner **Panton** est un designer danois du XXe siècle. Architecte de formation, il sillonera l'Europe pour former sa culture du design et créer son propre studio. Considéré comme avant-gardiste, Verner créera tout au long de sa vie de nombreux objets qui aujourd'hui, font partie de la culture classique du design. Il travaillera principalement le plastique peu connu à l'époque comme unité de création atypique. Il mettra en avant ses oeuvres de par leurs formes affirmées et couleurs vives. 

En 1956, Panton participe à un concours lancé par un fabricant de mobilier et commencera à réfléchir à une chaise où assise, dossier et piètement sont faits d’une seule pièce. Le processus de réflexion de la **Panton Chair** est lancé. Entre temps, il est reconnu dans toute l'Europe comme l'un des pionniers de la culture design pop. C'est en 1963, lors de sa encontre avec Willi **Fehlbaum**, dirigeant de Vitra qu'il décide de continuer ses recherches sur sa chaise en porte-à-faux d'un seul tenant. S'en suit l'histoire de la **Panton Chair**

Pour plus d'informations, voici le lien d'un article sur l'hisoire de ce grand monsieur du design : [PORTRAIT VERNER PANTON](https://ideat.thegoodhub.com/2020/04/17/portrait-verner-panton-1926-1998-le-futuriste-danois/)

## Historique

Dans les années 1950, Panton fait une série de croquis et de dessins pour la Panton Chair. Verner dessinera pour la première fois la **Panton Chair** que nous connaissons en 1959 et en 1960, il crée son premier modèle, en plâtre moulé. Véritable classique de l’histoire du mobilier, elle sera développée pour la production en série en collaboration avec Vitra en 1967. Cette chaise est la première en plastique moulée d’une seule pièce. Conçue en plastique durable avec une finition mate, le choix de ce matériau permet de garantir un certaine souplesse à l'ensemble.

Extrait de l'article sur la Panton Chair de Vitra : _" Travaillant en étroite collaboration avec Fehlbaum, Panton produit un modèle pressé à froid fait de polyester, renforcé de fibre de verre. Ce premier modèle est assez lourd, et nécessite un travail de finition. Le modèle est ensuite amélioré par l’utilisation de polystyrène thermoplastique, ce qui conduit à une nette réduction des coûts étant adapté à la production industrielle. En 1968, Vitra lance la production en série de la version finale. Le matériau utilisé était le Baydur, une mousse de polyuréthane haute résilience produite en Allemagne. La chaise est ensuite vernie et offerte dans une gamme de sept couleurs "._

La production de la chaise se stop en 1979 car l'utilisation du matériau n'était pas adapté et vieillissait mal. C'est en 1983 que la production reprend et cette fois, l'utilisation de la mousse de polyuréthane est une alternative aux problèmes rencontrés dans le passé. Enfin, en 1999, Vitra utilise pour sa fabrication le plastique polypropylène.

![](images/Panton_chair_série.jpg)

Lien vers l'article de Vitra : [HISTOIRE PANTON CHAIR](https://www.vitra.com/nl-be/product/panton-chair)

# IDEOLOGIE DU PROJET

## Introduction

Pour amorcer le travail lié au projet personnel, on nous a proposé de choisir terme au travers d’une liste de 5 mots et d’effectuer des recherches sur ce dernier. C’est par ce processus de réflexion que nous définirons le mode de pensé qui guidera l’évolution de notre travail. On nous a fourni plusieurs définitions des différents termes proposés et je commence donc par choisir le terme qui me correspond le plus…

On nous a donc proposé les termes influence, référence, hommage, inspiration et extension.

J'ai dès lors décidé de m'intéresser à deux termes. à la fois l'influence qui fait plus référence à un processus immatériel et intériorisé. Dans un second temps, l'influence dont la porté est beaucoup plus matériel et physique. A que mon projet de référence s'est inspiré d'un autre projet détaillé ci-après.

## L’influence qu’est-ce que c’est ?

**Wiktionnaire**

_Action d’une personne, d’une circonstance ou d’une chose qui influe sur une autre._ 

**Le Petit Robert**

_Action exercée sur quelqu’un ou quelque chose. L'influence est donc due comme un effet : de l'éducation sur la personnalité par exemple._

_Action volontaire ou non sur quelqu’un. Termes en lien : ascendant, empire, emprise, pouvoir… Elle a une mauvaise influence sur lui par exemple._
 
_Pouvoir social d'une personne qui amène les autres à se ranger à son avis. Termes en lien : autorité, crédit… Avoir beaucoup d’influence par exemple._

**Larousse**

_Action, généralement continue qu'exerce quelque chose sur quelque chose ou sur quelqu’un : L'influence du climat sur la végétation. L'influence de la télévision sur les jeunes._

_Ascendant de quelqu'un sur quelqu'un d’autre : Il a beaucoup changé sous l'influence de son ami._

_Pouvoir social et politique de quelqu'un, d'un groupe, qui leur permet d'agir sur le cours des événements, des décisions prises, etc : On a vu grandir son influence dans le monde des affaires._

**Littré**

_Sorte d'écoulement matériel que l'ancienne physique supposait provenir du ciel et des astres et agir sur les hommes et sur les choses. Action qu'un corps électrisé exerce à distance sur un corps à l'état naturel._ 

_Action qui s'exerce entre des personnes ou des substances. Les éléments, la nourriture, la veille, le sommeil, les passions ont sur vous de continuelles influences._

_Autorité, crédit, ascendant, en parlant des personnes. C'est un homme sans influence dans le gouvernement par exemple._

Comme nous avons pu le voir en lisant les nombreuses définitions de ce terme, l’**influence** fait systématiquement référence à une notion de pouvoir. Le pouvoir qu’exerce un élément sur un autre, qu’il soit voulu ou non. Nous entrons dès lors dans une dynamique de contrôle. Dans notre société, la théorisation de ce terme est utilisée dans tout les domaines. Pour ne pas citer les exemples vus plus haut, le publicité à pour but d’influencer les masses afin de vendre leurs propos. 

Je me suis renseigné afin de savoir ce qu’évoquait ce terme pour mon entourage et de nouveau, l’influence est aujourd’hui perçue comme quelque chose de négatif en lien avec le pouvoir. «  Mon fils est influencé par un autre à l’école…  », « Cesse de m’influencer avec tes propos … ». Je pense qu’on a perdu la subtilité et la complexitté de ce qu’est l’influence. 

J’aime à penser que l’influence touche tout le monde et à des échelles différentes. Tout objet, personne, … aussi petite soit-elle, exerce une influence sur ce qui les entoure de par leur être. C’est donc pourquoi l’objet choisi influence déjà mes pensées et mes réflexions de projet. Je suis d’avis qu’il faut se nourrir de l’historique, du processus de création et tout ce qui constitue l’objet comme source de travail. Suite à cette analyse, le but du jeu n’est pas de travailler à la manière de mais plutôt de se nourrir d’une réflexion personnelle et d’une intégration de ensemble qui influenceront naturellement mes prises de position face au projet. Pour illustrer cette pensée, je dirais que mon precessus d'intégration et de création a débuté dès mon premier contact avec l'objet. Comme expliqué plus haut, La **PANTON Chair** fait référence à mon enfance et influencera ma réalisation et ma réflexion face au projet. D'autres éléments entreront donc naturellement en compte comme les termes jeunesse, le contexte, ...

## L'inspiration qu’est-ce que c’est ?

Pour ce terme, je me suis basé sur une définition classique du mot. Dans la Larousse, il diront que l'influence c'est : _Prendre quelqu'un pour modèle de son action, trouver des idées, des exemples chez quelqu'un, dans quelque chose : Un poète qui s'inspire des surréalistes_. C'est précisément cette démarche qui va guider le processus de réflexion du projet.

# RECHERCHES

## Processus de réflexion global

Divisons dans un premier temps la **Panton Chair** en plusieurs axes de réflexion.

![](images/Chaise.jpg)

- **Points importants**
  - **Courbure comme structure porteuse**
  - **Adaptation du moule à un usage**
  - **Uniformisation de l'ensemble**
  - **Resistance du matériau utilisé**

## Choix de l'objet à réaliser

Une fois les points importants de la référence mis en lumière, il me fallait choisir quel type d'objet je voulais réaliser. Il me faut donc traiter d'un objet à pieds comme la chaise qui supporte un support. Ce processus permet ainsi de garder un rocessus de composition similaire à celui de **Verner**. De plus lorsqu'on réalise une chaise, un pense automatiquement à la table qui va avec. Je me suis donc tourné vers la réalisation d'une table basse plus adapté à l'échelle imposée du projet.

![](images/table.jpg)

## Les courbures comme structure adaptée

Une fois l'objet choisi, je me suis penché sur le processus structurel de la table. Pour la Panton Chair, les pieds de la chaise ont été repensé comme un élément unique dont la portance est générée par les courbes du projet. Afin de comprendre ce processus et adapter cette thématisation de la courbe à mon projet une série de croquis de compréhension ont été réalisés.

![](images/Courbes.jpg)

En générant le profil coupé de la chaise nous pouvons comprendre l'allure de cette structure particulière. Travaillant par le vide, la chaise met en place une série d'entonnoir comme le montre le dessin ci-dessus. C'est le travail de ces courbes qui généreront l'empiettement de ma table. 

Avant de réaliser le profilé de mon objet, ma réflexion s'est penchée sur l'usage de ces courbes. Dans le Projet de **Verner Panton**, il définit l'allur de la courbure de sa chaise sur le modèle de l'allure d'un homme assis. Ainsi, le corps moule la chaise. J'ai voulu requestionner ce processus mais adapté à des objets qui pourraient se retrouver sur une table : Livres, bouteilles, ...

![](images/Moule.jpg)

Ue fois tout ces éléments pris en compte, je me suis penché sur l'allure générale de mon objet en reprennant les points d'analyse évoqués plus haut.

![](images/courbure_1.jpg)

Pour le processus de création de la courbe, j'ai dabord réalisé le profilé de la table en synthétisant les éléments. j'ai ensuite utilisé l'outil " MODIFIER FACE ". L'ensemble du processus de réalisation est similaire à celui utilisé pour recréer la **PANTON CHAIR**. Je vous invite dès lors à vous rendre sur l'onglet FUSION 360 du site en cliquant sur le lien suivant : [CONCEPTION des COURBURES](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/modules/Module%2002%20-%20FUSION%20360%20%28Conception%20assistée%20par%20ordinateur%29/)

Nous voyons donc la synthétisation des éléments. A l'image de la réflexion de Verner, Les courbure du projet deviennent la structure porteuse de l'élément. Ensuite ces courbures sont adaptées à l'usage que nous allons en faire. Il s'agit ici de questionner l'usage de cette table. Ranger ses clefs, sa bouteille, sont bic, ...

## Quel matériau utiliser ?

**LE CARTON**

![](images/Carton.jpg)

Mais pourquoi choisir le carton si éloigné lu plastique? Si on se réfère à l'historique de la Panton Chair, cette dernière voit le jour à une époque où ce matériau était en pleine explosion. Ne connaissant que très peu ses capacités de résistance, Verner traitera du porte à faux en poussant ses prototypes de plus en plus loin. Si nous résumons les points importants de son processus de création :

  - **S'adapte au contexte**
  - **S'adapte à son époque**
  - **Questionnement sur les forme du matériau**
  - **Questionnement sur la résistance du matériau**

Nous entrons dans l'ère du recyclage et de nouveaux matériaux sont sous utilisés aujourd'hui. Les enjeux sont ici multiple. Si nous utilisons le carton comme matériau principal, un réel questionnement se pose sur ses capacités à résister au différents poids ainsi que sur les formes possibles avec lui. Traiter la courbe avec le carton tout en gardant une certaine rigidité est difficile mais pas impossible. 

![](images/ref.jpg)

En se penchant sur les références ci-dessus, nous pouvons dégager deux façons de traiter le courbures. soit par l'encastrement au depit de la résistance soit par stratification ce qui augmente la capacité résistante de l'ensemble et traitant l'objet comme quelque chose d'unifié (en référence au projet de Verner Panton).

Nous nous inscrivons donc l'ère du temps et dans le carractère expérimentale qui ont carractérisé la création de la Panton Chair à l'époque.

Me nourissant de l'ensemble de ces réflexions et références, je m'atèle à la réalisation d'un prototypes test...

# REALISATION INITIALE

Pour la réalisation du projet, il faut donc penser l'objet comme un ensemble de segmentations verticales qui une fois additionnées formeront l'objet unifié. Nous passerons donc par une décomposition des courbes à la verticale. Nous modeliserons dès lors chaques tranches du projet sur le logiciel fusion.une approche globale avait déjà été réalisé plus haut. Ce dernier traitait l'objet commu une unité de départ dans laquelle nous creusons les courbes. Cette manière de travailler rappelle celle de la Panton Chair.

![](images/visu_1.jpg)

Nous travaillons ici par segmentation de l'élément. Chaques stratifications sont donc modélisées une à une. Par manque de définition de courbure il s'agit là d'une expérimentation d'idée. Il faudra décomposer les courbures par la suite en fonction du résultat que nous désirons. 

![](images/strat_2.jpg)

Après réalisation sur le logiciel, nous passons à l'expérimentation réelle. Une question subsiste quant à la rigidité de l'ensemble...

![](images/strats.jpg)

Une fois chaque élément coupé et décomposé, il n'y a plus qu'à les assembler dans l'ordre pour monter notre objet final. Il s'agit là d'un prototype qui va encore évoluer mais nous pouvons nous faire une idée générale de l'élément.

![](images/final.jpg)

 Nous pouvont voir sur le protoype la grossiereté de la stratification. J'ai pour réaliser ce prototype utilisé du carton classique à double cannelure. Mauvais chix car la courbe n'est plus très courbe. Il faudra donc réduire l'épaisseur du carton et tester à l'échelle 1:1 pour pleinnement percevoir ces courbes.
 Ces dernières devront être par ailleurs mieux réfléchis et adaptés à une fonction précise. 
 De manière générale, les principes créatifs et structurels mis en place ici fonctionnent. Il faut donc adapter le projet à l'échelle 1:1 et à ses utilisations.

Pour se faire une idée plus générale de ce que ça donnera à la bonne échelle, j'ai réalisé deux croquis qui traite du rapport entre la Panton Chair et cette table basse. Au travail ! 

 ![](images/Visu_2.jpg)

# REALISATION FINALE 

A venir ...
