![](images/Person_photo_accueil.jpg)

# HOME 

## A PROPOS DE MOI

Je m'appelle **Baptiste Sarazin**. Etudiant en Architecture, j'ai réalisé mon bachelier au sein de la facultée d'architecture de la _Cambre-Horta_. En cette belle année, j'ai choisis de nourir ma fomation d'une nouvelle vision du **numérique** au travers au travers du cours _Architecture & Design_. C'est donc par le biais de ce blog que je posterai mes réussites et échecs qui retraceront mon évolution dans ce nouveau domaine que je connais si peu. 

Il me semble judicieux de se décrire en quelques mots avant d'aller plus loin. Né à Lyon, j'ai depuis tout petit nourri une passion pour l'architecture et les relations humaines qui en découlent. Il nous est impossible de donner une définition établie de l’Architecture. En effet, cette dernière se nourrit de tellement d’éléments différents que sa construction, non dans le sens physique du terme, en devient la résultante d’un foisonnement indéchiffrable. Et c'est exactement cette complexité qui m'attire.

Petit voyageur à mes heures perdues ... ou non, ma famille s'est éparpillée au cours des années entre l'Angleterre, la France et la Belgique. Un beau matin, j'ai eu la chance de retrouver, enfoui dans la cave de mon grand-père son vieux vélo en pièces détachées. Depuis ce jour, il ne me quitte plus et me mène vers de nouveaux horizons de plus en plus éloignés. Ardennes, nord de la France, ... rien ne lui fait peur.

J'ai l'honneur de vous présenter mon plus fidèle destrier!

![](images/Photo_velo.jpg)

# l'Objet

Lors de notre première journée pour le cours d' _Architecture & Design_, nous avons été amené à visiter le musée **ADAM museum Art & Design**. Nous devions choisir un objet de l'exposition qui nous inspirait. Mon choix s'est porté sur la **PANTON CHAIR**, véritable classique de design. Pensé comme un seul objet et non comme plusieurs éléments imbriqué, cette réalisation m'a particulièrement touché. Cette fusion de la chaise où les éléments d'assise, le dossier et les piètements ne forment plus qu'un seul objet marque son carractère révolutionaire pour l'époque. C'est travers d'une analyse rigoureuse de cet objet que je réaliserai mon projet. Pour avoir plus d'information conernant l'histoire et l'analyse de la **Panton Chair**, veuillez vous référer à ce lien menant au module adéquat : [FINAL PROJECT - PANTON CHAIR](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/final-project/)

![](images/Panton_chair_série.jpg)
