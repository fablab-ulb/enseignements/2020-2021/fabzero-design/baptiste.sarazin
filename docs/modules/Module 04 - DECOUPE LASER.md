# DECOUPE LASER - Découpe assistée par ordinateur

## Introduction

Nous avons eu la chance de pouvoir suivre cette semaine la formation concernant la **découpe laser**. Nous avons l'opportunité d'utiliser deux machines au sein du **Fablab**, la **Laserzsaur** plus grande ainsi que la **Fullspectrum** plus petite.  Ayant déjà eu affaire à cette dernière au cours de mon cursus et du cours de projet, j'ai trouvé cette dernière très enrichissante. On nous à proposé pour l'exercice de la semaine de créer à laide du **laser cut**, une lampe ayant un lien avec la référence choisie au début de la formation. 

## Mesures pratiques

Avant de commencer les explications liées au projet, il me semble important de spécifier quelques attentes et précautions liées à l'usage de cette machine. Nous nous pencherons uniquement sur l'utilisation de la **Lasersaur**. 

**LA LASERSAUR QU'EST-CE QUE C'EST ?**

Comme expliqué sur le site dont je vous met le lien à la fin du parragraphe, la machine est une imprimante laser opensource réalisable par chacun. _ "La lasersaur est une découpeuse laser opensource dont les seuls composants non standards sont des panneaux en acryliques découpées au laser ainsi que des pièces d'assemblage mécanique découpées au laser dans de l'aluminium. Toute la partie mécanique est composé de profilé d'aluminium avec des pièces d'assemblage et d'autres pièces standards. L'électronique est conçue pour être contrôlée via une interface web. "_

Lien : [MONTAGE D'UNE LASERSAUR](https://fablabo.net/wiki/Montage_d%27une_Lasersaur)

Lors de notre formation, on nous à mis en garde sur quelques points intéressants à retranscrire. Il faut bien entendu faire attention à plusieurs aspects avant toute impression.

- Se renseigner sur l'ensemble des matériaux possible et les utiliser en connaissance.
- Ne jamais oublier d'ouvrir la vanne d'air comprimé ainsi que l'extracteur de fumée.
- Savoir où se trouve le bouton d'arrêt d'urgence pour palier à toute éventualité de problème.
- Avoir à proximité de quoi éteindre d'éventuelles flammes au sein de la machine lors d'une impression.

**MARCHE A SUIVRE**

Avant de commencer toute impression, il faut suivre certaines règles pour bien allumer et mettre en marche la machine de manière à éviter tout problème lors du travail. Dans un premier temps, allumer la **Lasersaur** en appuyant sur le bouton rouge positionné à droite de la machine. Nous pouvons la voir sur la photo suivante.

![](../images/Bouton_on_machine.jpg)

Une fois la macine activée, il ne faut surtout pas oublier, comme indiquer dans les précautions d d'allumer le refroidisseur dans un premier temps puis l'extracteur de fumée. afin d'éviter que lors de l'impression l'ensemble brûle et abime notamment la lunette du laser. Ca permet d'éviter aussi une accumulation de fumée au sein de la machine naussive pour la santé et pouvant nuir aussi à l'impression. 
Nous activerons la vanne d'air en tournant cette dernière pour qu'elle se retrouve parrallèle au tuyeau.

![](../images/Refroidisseur.jpg)

![](../images/Extracteur_de_fumée.jpg)

![](../images/Vanne.jpg)

## L'objet

Lors de notre première journée pour le cours d' _Architecture & Design_, nous avons été amené à visiter le musée **ADAM museum Art & Design**. Nous devions choisir un objet de l'exposition qui nous inspirait. Mon choix s'est porté sur la **PANTON CHAIR**, véritable classique de design. Pensé comme un seul objet et non comme plusieurs éléments imbriqué, cette réalisation m'a particulièrement touché. Cette fusion de la chaise où les éléments d'assise, le dossier et les piètements ne forment plus qu'un seul objet marque son carractère révolutionaire pour l'époque. 

![](../images/Panton_chair_série.jpg)

**REFLEXION**

Le travail de réinterprétation partir de cette chaise est essentiel. J'ai débuté en me remémorant les différents mots clefs réalisés lors de la phase d'analyse de l'objet. Plastique - Courbes - Porte-à-faux - Mouvement - Couleurs vives - Fusion - Elégance - Souplesse - Moule - ...J'ai décidé de travailler sur les courbes et le portafaux relatifs à la chaise de Panton. Pour débuter la conception de la lampe, j'ai réalisé des croquis d'étude pour comprendre leus principes structurels et esthétiques.


**PRODUCTION 2D**

une fois le design de l'objet choisi, je me suis penché sur sa décomposition 2D. Pour ce faire j'ai réalisé un fichier autocad élaborant cahques parties à découper par la suite. Une fois l'objet décomposé et le fichier autocad finalisé, il faut l'enregistrer au format **.DXF** en suivant cette démarche sur Mac. Cliquer sur l'onglet **FICHIER**, puis **ENREGISTRER SOUS** et choisir le bon format.

![](../images/Lampe.jpg)

Une fois le fichier **.DXF** à porté de main, nous pouvons l'ouvrir dans le logiciel **INKSCAPE **, ce qui nous permettra de vectoriser l'ensemble. Si les couleurs des différentes découpes n'ont pas été réalisées sur **Autocad**, nous pourrons les gérer ici. Il est important de noter que les traits de découpes doivent être réalisés dans un calque et une couleur bien précise et les traits gravés dans un autre.Il ne reste plus qu'à enregsitrer le fichier au format **.SVG** compatible avec la **LASERSAUR** et entamer le processus d'impression.

Lien de téléchargement : [INKSCAPE](https://inkscape.org/fr/release/inkscape-0.92.4/)

**IMPRESSION**

Nous nous interessons maintenant à l'impression. Nous placerons le fichier .SVG sur une clef usb que nous introduirons sur l'ordinateur de la fac lié à la machine. Dans un second temps, il faudra allumer le logiciel lié à la **LASERSAUR**. Cliquer sur **OUVIR**, sélectionner le bon fichier dans sa clef usb et le dessin apparaitra sur l'interface. Il faut gérer les couleurs dans l'onglet à gauche de l'interface en prenant soin de choisir pour le temps 1 les gravure et pour le temps 2 les découpes. la puissance pour découper du polypropilène de 0,7 mm accessible à tous à la fac est de 46 tandis que pour graver 20.

![](../images/Coupe.jpg)

![](../images/Decoupe.jpg)

**1ER TENTATIVE**
 
Dans un premier temps, pour comprendre la forme de la lape j'ai uniquement réalisé une découpe fine sans assise afin de générer les torsion et points d'accroche manuellement. Comme nous pouvons le voir, plusieurs aspects, volontairement non pris en compte se détache de cette première tentative. La tension de la feuille courbé n'est pas gérée mais intéressante de par la réflexion qu'elle engendre sur la stabilité et la rigidité de l'ensemble.

![](../images/test_1.jpg)

**2EME TENTATIVE**

Suivant le même schémas, je me suis intéresser au la structure d'assise de la lampe visiblement peu adéquate. Afin de maintenir la structure bombé de la lampe je me suis penché sur l'élaboration d'un socle. Le résultat est en cours de réflexion mais l'idéologie de l'ensemble semble être bon. Nous ne garderons juste le caractère unique de l'objet.

![](../images/124925955_799014100884627_4901640432418301734_n.jpg)

**3EME TENTATIVE**

Pour cette tentative, je me penche sur une restructuration de mes idiées. pour générer une lampe cette fois ci non courbée. Pour ce faire garder la même réflexion mais utilisant des découpes et formes géométrique sismples qui garantiront un meilleures stabilitié de l'ensemble et une meilleure hiérarchie de l'ensemble comme sur ce croquis d'une lampe déjà existante.

En cours de réalisation ...


