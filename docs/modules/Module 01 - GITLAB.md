# GITLAB

## Introduction

Au cours de cette première semaine de formations liées à la **Fab Academy**, nous avons eu la chance de suivre deux présentation. La première portait sur l'utilisation du logiciel **FUSION 360**, détaillée dans le [MODULE 2](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/modules/module02/) de ce blog et la deuxième sur l'utilisation de **GITLAB**. On nous a donné les bases du codage en **Markdown** que nous utilisons pour réalisé notamment ce site.

La **Fab Academy** est liée à la fondation internationale des FabLabs. Le contenu de la formation proposée est basée sur le célèbre cours de prototypage rapide du **MIT** intitulé _How to Make (Almost) Anything_. Nous retrouverons sur leur site un définition assez claire de cette formation : " _La Fab Academy propose un enseignement basé sur  un modèle distribué plutôt que à distance : les étudiants apprennent dans des groupes de travail locaux, avec des pairs, des mentors et des machines. Ces groupes sont ensuite connectés globalement pour partager les contenus et participer à la classe interactive vidéo de manière collective (Mondial). Chaque Fab Lab est suivi et supervisé  au niveau régional par des sites de supernodes dotés de capacités, d’une expertise et d’inventaires plus avancés "._ Le but de cette formation est de former des **fabmanagers** qui eux formeront des fabmanagers qui eux à leur tour formeront des fabmananagers, etc. De cette manière, ce réseau s'autogère et s'aggrandit de lui même.

## GitLab, Qu'est ce que c'est?

Il s'agit une plateforme permettant d’héberger et de gérer un ensemble de projets web. Présentée comme la plateforme des développeurs modernes, elle offre la possibilité de gérer ses dépôts **Git** et ainsi de mieux appréhender la gestion des versions de vos codes sources. Mais dans le fond, Ce qui est d'autant plus intéressant c'est qu'il est gratuit en **Open Source** et collaboratif.
Si vous n'êtes pas familier avec l'utilisation du GitLab, voici un lien qui explique de A à Z les potentialté de cette plateforme : [INFOS GITLAB](https://blog.axopen.com/2017/02/gitlab-cest-quoi/) 

C'est donc directement sur le site gitlab que nous serons amené à créer notre blog et de ce pas partagé l'ensemble des capacité acquises lors de ces différentes formations. Ci joint le lien vers le site **GitLab** : [GitLab.com](https://gitlab.com)  

![](../images/GitLab_Accueil.jpg)

## Modifier son Git

Pour modifier son Git, la marche à suivre n'est pas compliquée. Tout d'abord, il faut se rendre sur le site GitLab.com et s'enregistrer. Ensuite se rendre sur sa page personnel, cliquer sur **DOCS** et modifier son site.

![](../images/Git_perso.jpg)

![](../images/DOCS.jpg)

Une fois dans l'onglet DOCS, nous pouvons importer des photos dans le dossier **IMAGES** en cliquant dessus puis en cliquant sur le **+** et **UPLOAD FILES**. Ces dernières pourront dès lors être utilisées dans l'élaboration de notre site comme l'image suivante.

![](../images/Upload_files.jpg)

Pour modifier directement son Git, c'est dans les onglets **MODULES**, **FINAL PROJECTS** et **INDEX** que cela se passe. nous cliquons dessus et pouvons éditer directement à partir de la plateforme le contenu de chaque onglet. Par exemple pour écrire ce texte que vous lisez et y ajouter des photos, il suffit de cliquer sur l'onglet **MODULES**, puis **MODULE 01 - GITLAB** et enfin **EDITER** comme imagé ci-après.

![](../images/Module_01_-_GITLAB.jpg)

![](../images/EDIT.jpg)

![](../images/Dans_EDIT.jpg)

## Bases du codage

Une fois lancé dans l'**EDIT** de son site, il faut bien entendu appliqué des conventions de codage relatives au système **Markdown**. Pour mieux comprendre certaines lignes et éviter de les chercher trop longtemps, voici les principales et les plus élémentaires qui m'ont permis de réaliser le mien.

![](../images/Bases_du_MD.jpg)

Il est intéressant de noter par ailleurs deux lignes de code non reprise dans ce petit résumé qui me semble être essentielle à la création de tout contenu numérique. Ces dernières sont bien entendu spécifiques au **Markdown** et donc au cas qui nous concerne.

Pour insérer une vidéo dans son **Git**, il suffit d'importer sa vidéo dans le dossier images ici, et d'appliquer la ligne de code classique utilisée pour une image à savoir : **![](../images/Nom de la vidéo.format)**

Pour utiliser un mot comme lien redirigeant vers une adresse extérieure au **Git**, il suffit d'utiliser cette ligne de code ci : **[](Adresse URL de l'adresse sur laquelle il faut être dirigé)**

Vous trouverez par ailleurs sur le site **OPENCLASSROOMS** ou **IONOS** les bases de l'écriture en Markdown très utile : [OPENCLASSROOMS MARKDOWN](https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown) & [IONOS MARKDOWN](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/markdown/).

## Clef SSH

En cours de réalisation

## Consulter sa page 

Une fois sa plateforme éditée, vous pouvez vous rendre dessus en cliquant sur l'onglet **SETTINGS** et le sous onglet **PAGES** de la barre de recherche située à gache du site. Vous serez redirigez vers une nouvelles pages sur laquelle vous pourrez retrouver le lien vers votre plateforme personnelle. Dans mon cas il s'agit de celui-ci : https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/ 

![](../images/Page_Personnelle.jpg)



