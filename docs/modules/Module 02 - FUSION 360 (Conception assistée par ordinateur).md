# FUSION 360 - Conception assistée par ordinateur

![](../images/FUSION_LOGO.jpg)

## Introduction

Pour ce premier cours lié à la modélisation 3D, nous avons suivi une formation concernant l'utilisation du logiciel **FUSION 360**. Fusion 360 est disponible via la license **Autodesk** bien connue des architectes utilisant **Autocad**. Nous trouverons cette description sur leur site internet qui traduit l'idéologie du logiciel : _" Fusion 360 a été conçu depuis le départ comme une solution couvrant tout l'ensemble du développement de produits. Il vise à proposer un processus de travail simple, enchainant toutes les étapes allant de la conception jusqu'à la fabrication industrielle "_.

Dans notre cas, nous bénéficions de la version étudiante d'**Autodesk** nous donnant accès à l'ensemble gratuitement. Voici tout de même le lien vers le site au besoin : [FUSION 360](https://www.autodesk.com/products/fusion-360/overview).

## Exercice

L'exercice de la semaine était de réaliser la modélisation de notre objet de réference choisi lors de la visite du musée **ADAM museum Art & Design**. Dans mon cas il s'agit de la PANTON CHAIR designée par Verner Panton. J'ai donc contruit l'objet à partir du logiciel Fusion 360. Nous allons voir ci dessous l'ensemble du processus de création de la chaise ainsi que les différents outils utilisés pour la réaliser.

![](../images/PANTON_CHAIR.jpg)

## TEMPS 1 - Réalisation de l'Esquisse générale

Pour débuter, il faut tout simplement créer une esquisse afin de commencer à dessiner sans oublier de choisir le plan vertical.

![](../images/Créer_un_Esquisse.png)

**1. LE PROFIL**

Dans le processus de création de notre chaise en 3 dimensions, j'ai pris le parti de construire dans un premier temps l'allure générale de la chaise. Pour ce faire, il existe deux manière de la réaliser. Soit on la construit directement à partir du logiciel en question à l'aide des outils **Ligne** et **Spline**, soit on peut utiliser Autocad, plus facile pour construire l'objet en coupe puis exporter cette dernière dans Fusion.

![](../images/Ligne.png)

![](../images/Spline.png)

![](../images/Chaise_Profil.png)

Une fois le profil de la chaise dessiner, on appuiera sur **TERMINER L'ESQUISSE** pour passer de la 2D à la 3D. Nous utiliserons l'outil **EXTRUDE** pour étirer la forme et ainsi obtenir la forme générale de la chaise comme démontré sur l'image qui suit. Nous obtiendrons la matière première que nous viendrons modeler par la suite.

![](../images/Extrude.jpg)

![](../images/Profil_extrude.png)

**2. L'OBJET**

La prochaine étape marque la réalisation du dossier arrondi. Pour se faire nous devons commencer par créer un nouveau plan vertical que nous placerons à l'arrère de la chaise. Pour se faire nous utiliserons l'outil **CONSTRUIRE** et sélectionnerons l'onglet **Plan en décalage**. Ce plan sera donc placé à larrière du dossier de la chaise en sélectionnant le point le plus reculé du dossier. 

![](../images/Création_nouveau_plan.jpg)

![](../images/Plan_Dossier.jpg)

Nous pouvons dès lors dessiner la partie à extruder du dossier. Pour se faire utiliser l'outil **CERCLE** et sélectionner l'onglet **Cercle, 2 points**. Une fois l'arc de cercle déssiné, nous lierons chaque points de panière à créer un forme fini. A partir de cette dernière, nous l'extruderons du profil de la chaise afin de supprimet les parties en trop et ainsi créer la forme arrondie du dossier de la chaise.

![](../images/Cercle.png)

![](../images/Cercle_Dossier_Extrude.jpg)

![](../images/Chaise_Dossier_arrondi.jpg)

La première étape de création est fini. Nous avons maintenant l'allure de notre chaise. Il reste à modeler l'objet à l'aide d'outil plus complexe définis ci-après pour générer la forme finale constituant par ailleurs la structure de la chaise.

## TEMPS 2 - Travail de la Forme

Cette étape marque la création de l'objet et dans sa dimension la plus spécifique. Le module est caractérisé par la création de torsions et de courbures complexes. Ces dernières permettent à la chaise de tenir toute seule sans suport tels que des pieds. Au delà de l'aspect ethétique de l'objet, nous pouvons définir son design comme structure. 

A l'aide de l'outil "MODIFIER UN FACE", nous pouvons choisir une face de notre objet sur laquelle viendra s'appliquer un maillage _(voir image ci-dessous)_. Une fois le maillage mis en place, nous pouvons jouer sur la curbure de l'objet en sélectionnant les points de jonction de du damier. Comme nous pouvons le voir sur les photos qui suivent, cet outil nous permet de répondre parfaitement à la méthode de création de l'objet étudié.

Pour plus d'information et de détail, j'ai suivi un tutoriel trouvé sur youtube. Je vous place le lien de la vidéo ici : [MAILLAGE SUR FUSION](https://www.youtube.com/watch?v=LcrD-MwguXY).

<iframe width="560" height="315" src="https://www.youtube.com/embed/LcrD-MwguXY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](../images/Fusion_360_outil_maillage.jpg)

![](../images/Fusion_360_maillage.jpg)


## TEMPS 3 - Finalisation

Une fois l'objet modélisé, il ne reste que les finition à faire. J'ai choisi d'arrondir les arrêtes de mon objet pour lui donner un aspect plus ondulé. A l'image de la forme de la chaise ce choix me semblait judicieux et entrant dans l'idéologie de la réalisation. Pour ce faire, nous utiliserons l'outil **CONGE**. une l'outil sélectioné, il suffira de cliquer sur les arrêtes que nous voulons arrondir et de rentrer la valeur de courbure. 

![](../images/Congé_outil.png)

![](../images/Congé.jpg)

Une fois cette dernière étape accompli, nous obtenons notre modélisation finie. Il ne reste dès lors plus qu'à aller s'amuser sur l'imprimante 3D. L'ensemble des étapes concernant l'impression de l'objet sont développée dans le module 03 du blog. Pour les voir il suffit de cliquer ici : [Module 01 : IMPRESSION 3D](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/modules/module03/).

## Autres Réalisations

**1. ADAPTATION PERSONNELLE DES CONNAISSANCES**

Une fois l'éxercice réalisé, quelle utilisation personelle peut-on en faire? La facilité d'éxécution nous permet de créer de nombreux objets qui faciliteront notre quotidien. Dans mon cas, ayant de nombreuse fois vu mes grands-parents perdus face à l'utilisation de leur smartphone. Comment le tenir? Comment taper sur leur clavier? Les conversation facetime où l'on voit sans cesse leurs pieds! Pour palier à ça, un support pour smartphone me semblait être une solution adéquate à leur non expérience dans ce domaine.

c'est à partir de fusion que j'ai désigné ce support de téléphone. Fort répendu je l'admet, il s'agissait ici, au delà de l'aspect utile, d'un éxercice personnel d'utilisation des connaissances apprises dans le cadre du cours. Suivant le même procédé, l'allure générale est généré de la même façon que l'éxercice précédent. Je vous invite donc à aller voir les premières étapes développées ci-avant car les même outils ont été utilisé. 

La particularité était d'intégrer dans ce cas un logo à la réalisation en utilisant l'outil **TEXTE** puis **EXTRUDE** afin de creuser le texte dans la structure du support. Pour ce faire l'intégration du logo ce fait sur une surfact oblique. Comme vu précédemment pour la chaise nous devons créer un nouveau plan qui épousera la surface voulue. Une fois fait, pour intégrer le texte, nous devons **CREER UNE ESQUISSE** et sélectionner le plan sur lequel intervenir.

![](../images/Outil_texte.jpg)

![](../images/Support_Iphone_Perso.jpg)

**1. AUTRE REALISATION**

![](../images/DE.jpg)
