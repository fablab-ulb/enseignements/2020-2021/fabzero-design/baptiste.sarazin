## IMPRESSION 3D - DU NUMERIQUE AU REEL

## Introduction

Pour ce deuxième cours lié à l'impression 3D, nous avons suivi une formation concernant l'utilisation de la machine **PRUSA I3 MK3S**. Créée par _Josef Prusa_, il s'agit d'une série d'imprimante développée et accessible en open-source. Elles ont la particularité d'être auto-renouvelable, dans la mesure du possible. Chaque imprimante peut recréer chaque partie de cette dernière et est donc en supposition, auto-renouvelable. 

Deux éléments sont à prendre en compte lorsqu'on travaille sur de l'impression 3D. Dans un premier temps l'impression se fait à partir d'une modélisation réalisée antérieurement. Pour ce faire, nous travaillerons à partir du logiciel **FUSION 360**. Ce logiciel est expliqué plus en détail dans l'onglet _FUSION 360_ consultable via ce lien : [FUSION 360 - Conception assistée par ordinateur](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/modules/module02/). Dans un second temps, une fois l'objet modélisé, nous travaillerons sur les réglages de la machine à partir du logiciel _PrussiaSlicer_ disponible ici : [Télécharger PrussiaSlicer](https://www.prusa3d.com/prusaslicer/). Une fois ces éléments compris et acquis, nous essayerons de comprendre comment réaliser une impression 3D sans accroc au travers d'un exercice proposé au sein du cours.

## Exercice

L'exercice de la journée était le suivant, à la suite de la formation liée à la machine, nous devions designer un objet de réference choisis lors de la visite du musée **ADAM museum Art & Design**. Dans mon cas il s'agit de la PANTON CHAIR designée par Verner Panton. J'ai dans un premier temps contruit l'objet à partir du logiciel _Fusion 360_. Il fallait ensuite l'exporter sur _PrusaSlicer_ au format **.STL** et appliquer les réglages machines vus précédement. Une fois chaque réglage défini, il ne restait plus que le passage du numérique au physique. Pour ce faire, placer le fichier au format **.Gcode** sur la carte sd de l'imprimante, attendre que le support et la machine chauffe à bonne température puis lancer l'impression. L'ensemble de ces démarches sont détaillées dans la suite de du blog.

![](../images/PANTON_CHAIR.jpg)

## TEMPS 1 - Fusion 360

Cette étape marque la création de l'objet et trois dimensions directement sur **Fusion 360**. Le module est caractérisé par la création de torsion et courbures complexes. Ces dernières permettent à la chaise de tenir toute seule sans suport tels que des pieds de chaise. Au delà de l'aspect ethétique de l'objet, son design devient sa structure même. 

A l'aide de l'outil "MODIFIER UN FACE", nous pouvons choisir une face de notre objet sur laquelle viendra s'appliquer un maillage _(voir image 1)_. C'est à partir des points de jonction de ce maillage que nous allons pouvoir modifier la courbure de cette face. Comme nous pouvons le voir sur les photos précédentes, cet outil nous permet de répondre parfaitement à la méthode de création de l'objet étudié.

Pour plus de détail concernant la conception de l'objet, voici le lien vers le module adéquat : [FUSION 360 - Conception assistée par ordinateur](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/baptiste.sarazin/modules/module02/) 

![](../images/Fusion_360_outil_maillage.jpg)
_Objet réalisé à partir de fusion 360_

## TEMPS 2 - PrusaSlicer

**1. IMPORTER LA MODELISATION**

Une fois l'objet modélisé et l'échelle réglée, nous pouvons sortir le fichier sous format **.stl** compatible avec le logicielle **PrusaSlicer**. C'est à partir de ce logiciel que nous pourrons gérer les réglages de l'imprimante 3D à savoir taille de l'objet, support d'impression, ...

![](../images/Fusion_Exporter.jpg)

![](../images/Fusion_Format_STL.jpg)

Il suffira par la suite de glisser le fichier **.stl** dans le logiciel **Prusaslicer**. La modélisation 3D sera générée automatiquement et nous pourons dès lors commencé à régler les données d'impression. Pour commencer, nous pouvons modifier l'échelle de l'objet ou encore créer un rotation de ce dernier à l'aide de la barre d'outil située à gauche du logiciel. Cette dernière peut paraître anodine et pourtant... Il existe des cas où imprimer l'objet couché devient essentiel à une bonne impression et éviter tout accroc. En effet au plus l'objet aura de contact avec le plateau d'impression, au moins l'objet aura tendance à bouger pendant l'impression. Dans le cas ci-présent l'objet à été imprimé directement sur sa base.

![](../images/PS_Ecran_de_base_avec_Objet.jpg)

**2. REGLER L'IMPRESSION**

Nous pouvons dès lors nous intéresser au réglages d'impression. Il est impératif de noter l'importance de passer en mode **expert** ce qui nous permettra de profiter pleinement des fonctionnalités du logiciel.

![](../images/PS_Mode_expert.jpg)

Nous cliquerons ensuite sur l'onglet **Réglages d'Impression** visible sur l'image ci-dessus. Il y quelques catégories qu'il est important de détailler détaillées.

Dans un premier temps l'onglet **Couches et Périmètres** nous permettant de gérer notamment l'espacement entre chaques couche d'impression. 

Dans un second temps l'onglet **Remplissage** qui nous permettra de gérer le motif et la densité de remplissage de l'objet. En effet lor d'une impression, les creux générés à l'intérieur de l'objet doivent être combler. En fonction de nos attentes, nous choisirons un motif bien précs. Certains garantirons la rigidité de l'ensemble tandis que d'autre permettrons d'obtenir une certaine souplesse. Dans notre cas nous choisirons le _Giroïde_ ou le _Nid d'abeille_ conseillé pour obtenir un objet rigide.

Dans un troisième temps l'onglet **Jupe et Bordure** qui nous permettra de gérer l'accroche au plateau d'impression. Il faut bien faire attention à appliquer un support à l'objet en cochant l'onglet "Générer Support" car si ce dernier n'a pas une base assez grande et rigide avec le plateau l'objet pourrait être mal imprimé. En effet au plus l'objet aura de contact avec le plateau d'impression, au moins l'objet aura tendance à bouger lors de l'impression. Ainsi la bordure définira le périmètre d'impression et la jupe générera le support au sol retirable post impression.

![](../images/PS_Jupes_et_Bordure.jpg)

Pour finir, l'onglet **Support** nous permettra de générer des liaisions entre les éléments pour permettre une bonne impression. A l'image d'un échafaudage, ce dernier permettra une tenu d'ensemble. Les différents support ne sont pas imprimés avec la même densité permettant à la fin de l'impression de l'oter facilement. Nous pouvons observer sur l'image ci-dessous le support généré en vert autour de l'objet initial en jaune. On comprend donc la nécessité du support pour une bonne impression.

![](../images/Prusaslicer_Support.jpg)

![](../images/PS_Support.jpg)

Pour mieux comprendre les différentes étapes de réglage, je me suis référer au notes prises lors de la formation **PrusaSlicer** ainsi qu'à une série de vidéo trouvées sur la chaine **U Print** sur youtube dont voici le lien : [Chaine U Print](https://www.youtube.com/channel/UCWm_XpGgK8mMFSzu-21UBZA).

**3. FINALISATION**

Une fois réglages générés et adaptés, il n'y a plus qu'à sortir notre fichier au format **.Gcode** compatible avec l'imprimante. Pour ce faire il suffit de cliquer sur **Découper Maintenant**, ainsi sera généré la 3D avec ses support et bordure. Ensuite cliquer sur **Exporter le G-CODE** pour obtenir le fichier compatible avec l'imprimante.

![](../images/PS_Decouper_Maintenant.jpg)

![](../images/PS_Exporter_GCODE.jpg)

![](../images/GCODE.jpg)

## TEMPS 3 - La Machine, du numérique au réel

Une fois le fichier inséré sur la carte SD de la machine, nous pouvons lancer l'impression.  Pour ce faire sélectioner le fichier sur le pad de la machine. Il ne faut pas oublier, lorsque l'on passe sur la machine de bien vérifier la température de chauffe du plateau à savoir 60 degrés et la chauffe de la machine qui diffère en fonction du type de bobine utilisé, dans ce cas là 215 degrés. Il n'y a plus qu'à attendre la réalisation de l'objet...

![](../images/TIME_LAPSE_1ere_impression.mp4)

## TEMPS 4 - Obtention de l'objet

Retirer l'objet de la machine. Il n'y a plus qu'a retirer la jupe ainsi que les supports à l'aide d'une petite pince pour éviter de casser l'objet. N'ayant pas utilisé cette dernière pour faire ça avec soin, nous pouvons voir sur les images qu'un trou s'est créé au point faible de l'objet. pour palier à cela nous pouvons gérer la rigidité des points d'accroche dans l'onglet **Support** sur **PrusaSlicer**. Lors de ma deuxième tentative, il n'y a eu aucune complication comme présent sur la troisième photo.

Il existe aussi une autre manière de procéder non vu au cours. L'utilisation d'une double buse permettrait d'utiliser deux bobines. L'une constitue l'objet voulu de manière classique tandis que l'autre buse utilise une bobine qui se dissout au contact de l'eau. Ainsi le support généré par cette bobine se disoudrait plus facilement et éviterait tout problème lors de cette étape.

![](../images/CHAISE_TEST_1.FINAL.jpg)

![](../images/CHAISE_TEST_1.FINAL2.jpg)

## Autres Réalisations

**1. ADAPTATION PERSONNELLE DES CONNAISSANCES**

Une fois l'éxercice réalisé et la porte vers cette nouvelle technologie d'impression ouverte, quelle utilisation personelle peut-on en faire? La facilité d'éxécution nous permet de créer de nombreux objets qui faciliteront notre quotidien. Dans mon cas, ayant de nombreuse fois vu mes grands-parents perdus face à l'utilisation de leur smartphone. Comment le tenir? Comment taper sur leur clavier? Les conversation facetime où l'on voit sans cesse leurs pieds! Pour palier à ça, un support pour smartphone me semblait être une solution adéquate à leur non expérience dans ce domaine.

Suivant le même procédé que pour l'éxercice précédent, c'est à partir de fusion que j'ai désigné ce support de téléphone. Fort répendu je l'admet, il s'agissait ici, au delà de l'aspect utile, d'un éxercice personnel d'utilisation des connaissances apprises dans le cadre du cours. La particularité était d'intégrer dans ce cas un logo à la réalisation en utilisant l'outil "TEXTE" puis "EXTRUDE" afin de creuser le texte dans la structure du support.

![](../images/Support_Iphone_Perso.jpg)

Au cours du début de l'impression, la machine s'est arrêtée et affichait sur le pad "Z PROBLEM". Après avoir fais deux trois recherches sur des forums, il s'avère que le probleme venait de l'axe Z soit l'axe vertical de la machine. Le problème rencontré était tout simple à régler mais néanmoins important de préciser. Lors du calibrage de la machine, le support était émenté sur les vis et était donc légèrement penché. Ce dernier ne pouvait donc se faire. Il faut donc bien faire attention à bien émenter le support au bon endroit et verifier que ce dernier soit bien plat.

**2. AUTRES EXEMPLES DE REALISATIONS**

![](../images/Prod_perso.jpg)
